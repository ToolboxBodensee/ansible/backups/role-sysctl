Configuration for sysctl
========================

Ansible role to configure the kernel via `sysctl`.


Variables
---------

* `sysctl_snippets`:
  List of `sysctl` snippets to install. By default
  only `70-privacyextensions.sysctl.conf` is enabled.


Files
-----

* `sysctl.conf`:
  The main `sysctl` configuration file.


Some ready made snippets included in this role:

* `60-icmpredirect.sysctl.conf`:
  Do not accept ICMP redirects (prevent MITM attacks)

* `70-privacyextensions.sysctl.conf`
  Enable IPv6 privacy extensions

* `80-pathfiltering.sysctl.conf`
  Enable Spoof protection (reverse-path filter)

* `90-forwarding.sysctl.conf`
  Enable packet forwarding (for routers)
